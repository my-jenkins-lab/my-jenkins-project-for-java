# Shoe-Shopping-Cart
Shoe Shopping Cart built with SpringBoot, JPA, MySQL , Spring Security, Hibernate and Thymeleaf

![image](https://user-images.githubusercontent.com/29988949/75882730-9ad11680-5dd6-11ea-9648-252426582a96.png)


![image](https://user-images.githubusercontent.com/29988949/75947593-c6dfac80-5e55-11ea-8582-bce667beb9bb.png)

`ProductList Page`

![image](https://user-images.githubusercontent.com/29988949/75968115-bf35fd00-5e81-11ea-9bae-e78ff047dcfd.png)

`Cart Page`

![image](https://user-images.githubusercontent.com/29988949/75956013-da960d80-5e6b-11ea-84b2-a0ca854ef9c9.png)

# Dockerfile and Docker compose

| Written by tpneik |

If you want to rebuild the db, uncomment the 11th line of docker compose file. Change the shoe_shopdb.sql as you desired. 

To run, type the following command: 

` docker compose up `